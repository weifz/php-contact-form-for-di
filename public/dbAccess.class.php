<?php

//declaring these outside the class since class constants would be public and visible.
define(DBUSERNAME, 'my_username');
define(DBPASSWORD, 'my_password');
define(DBSERVER, '127.0.0.1');

class dbAccess {

  public $db;
  private $objDB;

  function __construct() {

  }

  function dbConnect($newDB='diDB') {
    $dbLink = new mysqli(DBSERVER, DBUSERNAME, DBPASSWORD, $newDB);
    if ($dbLink->connect_errno) {
      echo "Error connecting to the {$newDB} database.";
      return false;
    }
    else {
      $this->db = $newDB;
      $this->objDB = $dbLink;
      return true;
    }
  }

  //Run a database query
  function q($strQuery) {
    if (isset($this->objDB)) {
      $result = $this->objDB->query($strQuery);
      return $result;
    }
    else {
      return false;
    }
  }
}
 ?>
