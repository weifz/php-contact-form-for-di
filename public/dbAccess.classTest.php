<?php

require("dbAccess.class.php");

use PHPUnit\Framework\TestCase;

class dbAccessTest extends TestCase {

  private $objDB;

  function setUp() {
    $this->objDB = new dbAccess();
  }

  function tearDown() {
    $this->objDB = NULL;
  }

  function testdbConnect() {
    $result = $this->objDB->dbConnect();
    $this->assertEquals(true,$result); //Since we have no real u/p for a DB connection, this should fail.
  }

  function testq() {
    $strSQL = "select * from smileyEmails";
    $result = $this->objDB->q($strSQL);
    $this->assertEquals(true,$result); //Since we have no real u/p for a DB connection, this should fail also.
  }

  //Let's verify that we changed to the DB we specified
  function testDB() {
    $dbName = 'testDB';
    $result = $this->objDB->dbConnect($dbName);
    $this->assertEquals($dbName,$this->objDB->db); //For now, this still fails because we don't the u/p for a real DB connection
  }
}
 ?>
