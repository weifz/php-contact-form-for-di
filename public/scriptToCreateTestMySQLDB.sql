CREATE DATABASE  IF NOT EXISTS `diDB`;
USE `diDB`;

--
-- Table structure for table `smileyEmails`
--

DROP TABLE IF EXISTS `smileyEmails`;

CREATE TABLE `smileyEmails` (
  `id` int(11) NOT NULL,
  `fullName` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `telephone` varchar(12) CHARACTER SET latin1 DEFAULT NULL,
  `message` text CHARACTER SET latin1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
