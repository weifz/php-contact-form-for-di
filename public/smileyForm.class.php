<?php
require('dbAccess.class.php');
require('smileyMailer.class.php');

//I hate that I am creating a class so specific that it is mostly not reusable but it makes phpunit testing easier.
class smileyForm {

private $arrFields;
private $strSQLName;
private $strSQLValue;
private $arrEmailData;
private $objDB;

function __construct($arrRequest) {
  //These are our form fields we expect to get data from, as named in the sending JS on the form page. Update as needed when form changes
  $this->arrFields = array('email', 'fullName', 'telephone', 'message');
  $this->strSQLName = '';
  $this->strSQLValue = '';
  $this->arrEmailData = array();
  $this->objDB = new dbAccess();

  $arrSQLName = array();
  $arrSQLValue = array();

  //Prepping the arrays for use by the mailer and the database insert
  foreach ($this->arrFields as $value) {
    $arrSQLName[] = $value;
    $arrSQLValue[] = "'" . $this->objDB->real_escape_string($arrRequest[$value]) . "'"; //Must protect against SQL injection
    $this->arrEmailData[$value] = $arrRequest[$value];
  }
  $this->strSQLName = implode(',',$arrSQLName);
  $this->strSQLValue = implode(',',$arrSQLValue);
}


//Create a record of this email in our database table.
function saveEmail(){
  $strSQL = "insert into smileyEmails ({$this->strSQLName}) values ({$this->strSQLValue})";
  $result = $objDB->q($strSQL);
  return $result;
}

//We send the email and return a boolean to let the calling JS know if the email was successfully sent or not
function sendEmail() {
  $objMailer = new smileyMailer($this->arrEmailData);
  return $objMailer->send();
}

}
?>
