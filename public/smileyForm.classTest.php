<?php

require("smileyForm.class.php");

use PHPUnit\Framework\TestCase;

class smileyFormTest extends TestCase {

  private $objToTest;

  function setUp() {
    $arrRequest = array('email' => 'a@aol.com', 'fullName' => 'Mr. Ed', 'telephone' => '212-222-1234', 'message' => 'Hello Guy!');
    $this->objToTest = new smileyForm($arrRequest);
  }

  function tearDown() {
    $this->objToTest = NULL;
  }

  function testSendEmail() {
    $this->assertEquals(true,$this->objToTest->sendEmail()); //Since we have no real mail server set up, this should fail.
  }

  function testSaveEmail() {
    $this->assertEquals(true,$this->objToTest->saveEmail()); //Since we have no real u/p for a DB connection, this should fail.
  }

}
 ?>
