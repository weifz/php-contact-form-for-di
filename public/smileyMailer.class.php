<?php

class smileyMailer {
  //Class variables for the front-end form fields. Update as needed
  public $email;
  public $fullName;
  public $message;
  public $telephone;

  protected $subject;
  protected $to;

  function __construct($arrInitVals) {

    //Known input fields from the front-end form. Update as needed
    $arrFields = array('email', 'fullName', 'message', 'telephone');
    //Populate our form fields with data given to us
    foreach ($arrFields as $value) {
      if (isset($arrInitVals[$value])) $this->{$value} = $arrInitVals[$value];
    }

    $this->subject = "Website contact form email for Guy Smiley";
    $this->to = 'guy-smiley@example.com';
  }

  // Get the header data for our email
  protected function getHeader() {

    $strHeader = "From: {$this->fullName} <{$this->email}> \r\n" .
    "Reply-To: {$this->email} \r\n" .
    "Content-Type: multipart/alternative";

    return $strHeader;
  }

  //A method for sending the email. All email data should be processed here in case any information was updated after the object was instantiated.
  function send() {

    return mail($this->to, $this->subject, $this->formatMessage(), $this->getHeader());

  }

  // create the HTML message and the text-only version for our email
  protected function formatMessage() {
    $textMsg = "From: {$this->fullName} \r\n" .
    "Email: {$this->email} \r\n" .
    "Message: {$this->message}";

    $htmlMsg = "<html><head><title></title></head><body><div style='padding:15px;'>" .
    "<div><b>From:</b> {$this->fullName}</div>" .
    "<div><b>Email:</b> {$this->email}</div>" .
    "<div><b>Message:</b> {$this->message}</div>" .
    "</div></body></html>";

    $msgBoundary = sha1(uniqid());

    $mergedMsg = "
    MIME-Version: 1.0
    Content-Type: multipart/alternative; boundary={$msgBoundary}

    --{$msgBoundary}
    Content-Type: text/plain; charset=\"iso-8859-1\"
    Content-Transfer-Encoding: 7bit

    {$textMsg}

    --{$msgBoundary}
    Content-Type: text/html; charset=\"iso-8859-1\"
    Content-Transfer-Encoding: 7bit

    {$htmlMsg}

    --{$msgBoundary}--";

    return $mergedMsg;

  }

}

 ?>
