<?php

require("smileyMailer.class.php");

use PHPUnit\Framework\TestCase;

class smileyMailerTest extends TestCase {

  private $objToTest;

  function setUp() {
    $arrRequest = array('email' => 'a@aol.com', 'fullName' => 'Mr. Ed', 'telephone' => '212-222-1234', 'message' => 'Hello Guy!');
    $this->objToTest = new smileyMailer($arrRequest);
  }

  function tearDown() {
    $this->objToTest = NULL;
  }

  function testSend() {
    $this->assertEquals(true,$this->objToTest->send()); //Since we have no real u/p for a DB connection, this should fail.
  }

}
 ?>
